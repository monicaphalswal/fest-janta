from __future__ import absolute_import

from celery import Celery
from datetime import timedelta

app = Celery('FestJanta',
             broker='amqp://',
             backend='amqp://',
             include=['FestJanta.settings.tasks'])


# Optional configuration, see the application user guide.
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)


CELERYBEAT_SCHEDULE = {
    'rank-events-every-30-seconds': {
        'task': 'tasks.event_rank',
        'schedule': timedelta(seconds=30),
        'args': (16, 16)
    },
}

if __name__ == '__main__':
    app.start()