from django.conf.urls import patterns, include, url
from fest.views import *
from django.views.generic import TemplateView
from django.conf import settings
import os
from django.views.decorators.cache import cache_page
site_media = os.path.join(
	os.path.dirname(__file__), 'site_media'
)

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    #Home page
	url(r'^$', main_page),

    #account settings
    url(r'^settings/profile/$', profile_settings), 
    url(r'^settings/password/$', password_settings),
    url(r'^settings/college/(?P<college_slug>[^\s]+)/$', college_settings),
    url(r'^settings/fest/(?P<fest_slug>[^\s]+)/$', fest_settings), 
    url(r'^settings/$', user_settings),
    url(r'^edit-description/(?P<value>(\d))/(?P<vid>(\d+))/$', edit_description), #edit account settings

    #account login and registration
    url(r'^accounts/activate/(?P<activation_key>\w+)/$', confirm), #activation link
	url(r'^login/$', remember_me_login), #login view
    url(r'^reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
            reset_confirm, name='password_reset_confirm'), #forgot password reset link
    url(r'^reset/$', reset, name='reset'), #get email for forgotten password reset
    url(r'^resetdone/$', 
        'django.contrib.auth.views.password_reset_done'), #email sent for recovery
	url(r'^logout/$', logout_page), #logout from account
    url(r'^register/$', register_page), #Signup for an account
    url(r'^register/success/$', TemplateView.as_view(template_name =  'registration/register_success.html')), #Signup success message
    url(r'^get-description/$', get_description), #Check if an email is registered already or username already exists

    #ajax autocomplete requests
    url(r'^ajax/college/autocomplete/$', ajax_college_autocomplete),
    url(r'^ajax/fest/autocomplete/$', ajax_fest_autocomplete),
    url(r'^query_suggestions/$', query_suggestions), #search bar

    #Fest and college management
    url(r'^save/$', fest_save_page), #add a fest and a college

    #Ajax updating of pages
    url(r'^post-comments/(?P<event_id>(\d+))/$', post_comments), #Post a comment and put it in latest comment
    url(r'^follows/(?P<fest_id>(\d+))/$', user_follows), #(Post) Follow a fest
    url(r'^unfollows/(?P<fest_id>(\d+))/$', user_unfollows), #(Post) Unfollow a fest
    url(r'^followscollege/(?P<college_id>(\d+))/$', user_follows_college), #(Post) Follow a college
    url(r'^unfollowscollege/(?P<college_id>(\d+))/$', user_unfollows_college), #(Post) Follow a college   
    url(r'^followsuser/(?P<user_id>(\d+))/$', follow), #(Post) Follow a user
    url(r'^unfollowsuser/(?P<user_id>(\d+))/$', unfollow), #(Post) Unfollow a user
    url(r'^like-event/(?P<vid>(\d+))/(?P<event_id>(\d+))/$', like_event), #(Post) Like an event
    url(r'^dislike-event/(?P<vid>(\d+))/(?P<event_id>(\d+))/$', dislike_event), #(Post) Dislike an event
    url(r'^get-complete/(?P<event_id>(\d+))/$', event_detail), #Load an event's complete detail
    url(r'^upload-image/(?P<vid>(\d+))/$', upload_image), # (Post) Upload DP
    url(r'^uploaded-image/$', get_uploaded_image), # (Get) Get uploaded user, college or fest dp
    url(r'^upload-event-album/$', upload_event_album), # (Post) Upload an album image
    url(r'^uploaded-event-image/$', uploaded_event_image), #(GET) Get thumbnail of an image uploaded in an album 
    url(r'^uploaded-event-album/(?P<event_id>(\d+))/$', uploaded_event_album), # (GET) Get an album and put it in latest event
    url(r'^delete-event/(?P<event_id>(\d+))/$', delete_event), # (Post) Delete an event

    url(r'^admin/', include(admin.site.urls)),

    # College/fest specific pages
    # Fest pages
    url(r'^fest/(?P<fest_slug>[^\s]+)/followers/$', fest_followers, name='fest_followers'),# List of followers of a fest
    url(r'^fest/(?P<fest_slug>[^\s]+)/post/(?P<event_id>(\d+))/$', event_page, name="event_page"), # Shows a single event a fest
    url(r'^fest/(?P<fest_slug>[^\s]+)/$', fest_page, name="fest_page"), # Fest page

    # College pages
    url(r'^college/(?P<college_slug>[^\s]+)/followers/$', college_followers, 
                                                          name='college_followers'), # List of followers of a college
    url(r'^college/(?P<slug>[^\s]+)/$', college_page), # College page

    # Info Pages
    url(r'^about/$', about), #about page
    url(r'^feedback/$', feedback), #(POST)feedback/contact page

    # Site media
	url(r'^site-media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    
    # User pages
	url(r'^(\w+)/$', user_page), # Main User Page
    url(r'^(\w+)/followers/$', user_followers, name='user_followers'), # user's followers page
    url(r'^(\w+)/following/$', user_following, name='user_following'), # user's following page
    url(r'^(\w+)/collegefollowing/$', user_college_following, name='user_college_following'), # user's college following page
    url(r'^(\w+)/festfollowing/$', user_fest_following, name='user_fest_following'),# user's fest following page
)
