from django.contrib import admin
from models import *

class  CommentInline(admin.TabularInline):
	model  =  Comment
	extra  =  3

class EventAdmin(admin.ModelAdmin):
	list_display  =  ('pk', 'fest_', 'description', 'user', 'pub_date', 'upvotes_', 'downvotes_', 'rank')
	fieldsets  =  [
		(None, {'fields':  ['description', 'fest', 'user', 'rank']}),
		('Stub Information',  {'fields':  ['pub_date'], 'classes': ['collapse']}),
	]
	inlines  =  [CommentInline]
	list_filter = ['pub_date']
	search_fields = ['description']

class CollegeAdmin(admin.ModelAdmin):
	prepopulated_fields = { 'slug': ['name'] }
	list_display  =  ('name',  'get_absolute_url', 'user', 'pub_date')
	fieldsets  =  [
		(None, {'fields':  ['name', 'slug', 'user', 'description', 'image', 'website', 'location']}),
		('Date  information',  {'fields':  ['pub_date'], 'classes': ['collapse']}),
	]

class FestAdmin(admin.ModelAdmin):
	prepopulated_fields = { 'slug': ['name'] }
	list_display  =  ('name',  'college', 'get_absolute_url', 'user', 'pub_date', 'rank' )
	fieldsets  =  [
		('Fest details', {'fields':  ['name', 'slug', 'description', 'college', 'image', 'website']}),
		(None, {'fields': ['user']}),
		('Stub  information',  {'fields':  ['pub_date', 'rank'], 'classes': ['collapse']}),
	]


class PhotoAdmin(admin.ModelAdmin):
    list_display = ['album_pk_', 'album']



class FestFollowingAdmin(admin.ModelAdmin):
	list_display  =  ('user',  'follows_')

class UserFollowingAdmin(admin.ModelAdmin):
	list_display  =  ('user',)

class CollegeFollowingAdmin(admin.ModelAdmin):
	list_display  =  ('user',  'follows_')

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ["user", "pub_date", 'bio', 'education']
    list_filter = ["user", "image", "thumbnail", "thumbnail2"]

    def save_model(self, request, obj, form, change):
        if not obj.user: obj.user = request.user
        obj.save()

class UpvoteAdmin(admin.ModelAdmin):
	list_display  =  ('user',  'event_')

class DownvoteAdmin(admin.ModelAdmin):
	list_display  =  ('user',  'event_')


class StreamItemAdmin(admin.ModelAdmin):
	list_display  =  ('content_type',  'pub_date', 'user')

class FestStreamItemAdmin(admin.ModelAdmin):
	list_display  =  ('content_type',  'pub_date', 'fest', 'fest_')

admin.site.register(Event, EventAdmin)
admin.site.register(Fest, FestAdmin)
admin.site.register(College, CollegeAdmin)
admin.site.register(CollegeFollowing, CollegeFollowingAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(FestFollowing, FestFollowingAdmin)
admin.site.register(UserFollowing, UserFollowingAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Upvote, UpvoteAdmin)
admin.site.register(Downvote, DownvoteAdmin)
admin.site.register(StreamItem , StreamItemAdmin)
admin.site.register(FestStreamItem , FestStreamItemAdmin)