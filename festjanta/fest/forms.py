from django import forms
import re
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

class RegistrationForm(forms.Form):
  username = forms.CharField(
    widget=forms.TextInput(attrs={'id': 'username-input', 'class': 'fest_save_input','size': 40, 'placeholder': 'Pick a username'}), 
    max_length=30
  )
  first_name = forms.CharField(
    widget=forms.TextInput(attrs={'id': 'first-input', 'class': 'fest_save_input','size': 40, 'placeholder': 'First Name'}), 
    max_length=30
  )
  last_name = forms.CharField(
    widget=forms.TextInput(attrs={'id': 'last-input', 'class': 'fest_save_input','size': 40, 'placeholder': 'Last Name'}), 
    max_length=30
  )
  email = forms.EmailField(
    widget=forms.TextInput(attrs={'id': 'email-input','class': 'fest_save_input','size': 40, 'placeholder': 'Email'}), 
  )
  password1 = forms.CharField(
     widget=forms.PasswordInput(attrs={'id': 'password1-input', 'class': 'fest_save_input','size': 40, 'placeholder': 'New Password'})
  )
  password2 = forms.CharField(
     widget=forms.PasswordInput(attrs={'id': 'password2-input', 'class': 'fest_save_input','size': 40, 'placeholder': 'Re-enter Password'})
  )

  def clean_password2(self):
    if 'password1' in self.cleaned_data:
      password1 = self.cleaned_data['password1']
      password2 = self.cleaned_data['password2']
      if password1 == password2:
        return password2
    raise forms.ValidationError('Passwords do not match.')

  def clean_username(self):
    username = self.cleaned_data['username']
    if not re.search(r'^\w+$', username):
      raise forms.ValidationError('Username can only contain '
        'alphanumeric characters and the underscore.')
    try:
      User.objects.get(username=username)
    except User.DoesNotExist:
      return username
    raise forms.ValidationError('Username is already taken.')

class FestSaveForm(forms.Form):
  name = forms.CharField(
    widget=forms.TextInput(attrs={'id': 'fest_input', 'class': 'fest_save_input','size': 40, 'maxlength':50, 'placeholder': 'Fest name'})
  )
  college_name = forms.CharField(
    widget=forms.TextInput(attrs={'id': 'college_input', 'class': 'fest_save_input','size': 40, 'maxlength':50,'placeholder': 'College name'})
  )

class SearchForm(forms.Form):
  query = forms.CharField(
    label=u'Search:',
    widget=forms.TextInput(attrs={'size': 45})
  )

class PasswordResetForm(forms.Form):
  email = forms.EmailField(
    widget=forms.TextInput(attrs={'class': 'fest_save_input','size': 40, 'placeholder': 'Email'}), 
  )

class AuthenticationRememberMeForm(AuthenticationForm):
  """
    Subclass of Django ``AuthenticationForm`` which adds a remember me
    checkbox.
    
  """
  remember_me = forms.BooleanField(
    label="Terms&Conditions", initial = False, required = False
  )