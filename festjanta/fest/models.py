from django.db import models
from django.contrib.auth.models import User
import datetime
#from django.core.files import File
#from string import join
#from tempfile import NamedTemporaryFile
import os
#from os.path import join as pjoin
#from FestJanta.settings import MEDIA_ROOT
from django.utils.text import slugify

from django.contrib.contenttypes import generic
from django.template.loader import render_to_string
#from django.db.models import signals

from django.contrib.contenttypes.models import ContentType

#import re
from django.db.models.signals import post_save, post_delete  
from PIL import Image
from cStringIO import StringIO
from django.core.files.uploadedfile import SimpleUploadedFile
import math
from django.utils import timezone


class College(models.Model):
    name = models.CharField(unique=True, max_length=50, 
        help_text='Name of the college.'
    )
    slug = models.SlugField(unique=True)
    pub_date = models.DateTimeField(default=timezone.now)
    description = models.TextField(blank = True)
    image = models.ImageField(upload_to='site-media/media/collegeimages/', 
        default = 'site-media/college_default.jpeg' 
    )
    user = models.ForeignKey(User)
    website = models.URLField(blank = True)
    location = models.CharField(max_length=50, blank = True)
    thumbnail = models.ImageField(upload_to="site-media/media/collegeimages/collegethumbs/", default = 'site-media/college_default_big.jpeg')
    thumbnail2 = models.ImageField(upload_to="site-media/media/collegeimages/collegethumbs2/", default = 'site-media/college_default_small.jpeg')

    class Meta:
        verbose_name_plural = 'Colleges'

    def get_absolute_url(self):
        return "/college/%s/" % self.slug

    def __unicode__(self):
        return self.image.name
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(College, self).save(*args, **kwargs)

class Fest(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(unique=True)
    description = models.TextField(blank = True)
    user = models.ForeignKey(User)
    college = models.ForeignKey(College)
    pub_date = models.DateTimeField(default=timezone.now)
    image = models.ImageField(upload_to='site-media/media/festimages/', 
        default = 'site-media/fest_default.jpeg' 
    )
    rank = models.DecimalField(default = 0, max_digits=11, decimal_places=2)
    website = models.URLField(blank = True)
    thumbnail = models.ImageField(upload_to="site-media/media/festimages/festthumbs/", default = 'site-media/fest_default_big.jpeg')
    thumbnail2 = models.ImageField(upload_to="site-media/media/festimages/festthumbs2/", default = 'site-media/fest_default_small.jpeg')

    def __unicode__(self):
        return self.image.name

    def get_absolute_url(self):
        return "/fest/%s/" % (self.slug)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.college.name) + '-' + slugify(self.name)
        super(Fest, self).save(*args, **kwargs)

    def fest_rank(self, following, time, *args):
        now = timezone.now()
        if type(time) is int:
            diff = now - datetime.fromtimestamp(time)
        else:
            diff = now - time
        second_diff = diff.seconds
        rank = following/((second_diff+2)**(1.5))
        count = 0
        rank_sum = 0
        for vals in args:
            rank_sum += vals
            count +=1
        avg_rank = rank_sum/count
        return rank + float(avg_rank)

from datetime import datetime
epoch = datetime(1970, 1, 1)
def epoch_seconds(date):
    """Returns the number of seconds from the epoch to date."""
    td = date - epoch
    return td.days * 86400 + td.seconds + (float(td.microseconds) / 1000000)

def score(ups, downs):
    return ups - downs
class Event(models.Model):
    description = models.TextField(blank = True)
    fest = models.ForeignKey(Fest)
    pub_date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User)
    rank = models.DecimalField(default = 0, max_digits=3, decimal_places=2)

    def __unicode__(self):
        return self.description

    def fest_(self):
        return self.fest.name

    def upvotes_(self):
        return self.upvote_set.count()

    def downvotes_(self):
        return self.downvote_set.count()

    def post_rank(self, time, upvotes, downvotes):
        now = timezone.now()
        if type(time) is int:
            diff = now - datetime.fromtimestamp(time)
        else:
            diff = now - time
        second_diff = diff.seconds
        x= upvotes - downvotes
        if x>0:
            y = 1
        elif x==0:
            y = 0
        else:
            y = -1
        if abs(x)>=1:
            z = abs(x)
        else:
            z =1
        return math.log10(z) + ((y*second_diff)/45000)

    def hot(ups, downs, date):
        """The hot formula. Should match the equivalent function in postgres."""
        s = score(ups, downs)
        order = math.log(max(abs(s), 1), 10)
        sign = 1 if s > 0 else -1 if s < 0 else 0
        seconds = epoch_seconds(date) - 1134028003
        return round(order + sign * seconds / 45000, 7)

class Photo(models.Model):
    image = models.ImageField(upload_to='site-media/media/images/')
    thumbnail = models.ImageField(upload_to="site-media/media/images/thumbs/", blank=True, null=True)
    thumbnail2 = models.ImageField(upload_to="site-media/media/images/thumbs2/", blank=True, null=True)
    album = models.ForeignKey(Event)

    def create_thumbnail(self):
        # If there is no image associated with this.
         # do not create thumbnail
        if not self.image:
             return

        # Set our max thumbnail size in a tuple (max width, max height)
        THUMBNAIL_SIZE = (225,225)
        THUMBNAIL2_SIZE = (450,450)

        image = Image.open(StringIO(self.image.read()))

        
        image.thumbnail(THUMBNAIL2_SIZE, Image.ANTIALIAS)
 
        temp_handle = StringIO()
        image.save(temp_handle, 'jpeg')
        temp_handle.seek(0)
        suf = SimpleUploadedFile(os.path.split(self.image.name)[-1],
                 temp_handle.read(), content_type='jpeg')
        self.thumbnail2.save('%s_photo_thumbnail2.%s'%(os.path.splitext(suf.name)[0],'jpeg'), suf, save=False)

         # Convert to RGB if necessary
        if image.mode not in ('L', 'RGB'):
            image = image.convert('RGB')
 
         # We use our PIL Image object to create the thumbnail, which already
         # has a thumbnail() convenience method that contrains proportions.
         # Additionally, we use Image.ANTIALIAS to make the image look better.
         # Without antialiasing the image pattern artifacts may result.
        image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
 
         # Save the thumbnail
        temp_handle = StringIO()
        image.save(temp_handle, 'jpeg')
        temp_handle.seek(0)
 
         # Save image to a SimpleUploadedFile which can be saved into
         # ImageField
        suf = SimpleUploadedFile(os.path.split(self.image.name)[-1],
                 temp_handle.read(), content_type='jpeg')
         # Save SimpleUploadedFile into image field
        self.thumbnail.save('%s_photo_thumbnail.%s'%(os.path.splitext(suf.name)[0],'jpeg'), suf, save=False)

        

    def save(self, force_insert=False, force_update=False, **kwargs):
        """Save image dimensions."""
        super(Photo, self).save(force_insert, force_update, **kwargs)

        self.create_thumbnail()
        force_insert = False
        force_update = True

        super(Photo, self).save(force_insert, force_update, **kwargs)


    def __unicode__(self):
        return self.image.name

    def album_pk_(self):
        return self.album.pk

class Upvote(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    pub_date = models.DateTimeField(default=timezone.now)

    def event_(self):
        return self.event.pk

class Downvote(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    pub_date = models.DateTimeField(default=timezone.now)

    def event_(self):
        return self.event.pk

class Comment(models.Model):
    content = models.CharField(max_length = 200)
    event = models.ForeignKey(Event)
    user = models.ForeignKey(User)
    pub_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-pub_date']


class FestFollowing(models.Model):
    user = models.ForeignKey(User)
    follows = models.ForeignKey(Fest)
    pub_date = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('user', 'follows')

    def follows_(self):
        return self.follows.name

    def __unicode__(self):
        return self.follows.name

    
class CollegeFollowing(models.Model):
    user = models.ForeignKey(User)
    follows = models.ForeignKey(College)
    pub_date = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('user', 'follows')

    def follows_(self):
        return self.follows.name

    def __unicode__(self):
        return self.follows.name


class UserFollowing(models.Model):
    user = models.OneToOneField(User)
    follows = models.ManyToManyField('self', related_name='followed_by', symmetrical=False)
    pub_date = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.user.username

User.profile = property(lambda u: UserFollowing.objects.get_or_create(user=u)[0])



class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="person")
    image = models.ImageField(upload_to='site-media/media/userimages/', default = 'site-media/user_default.jpeg')
    thumbnail = models.ImageField(upload_to="site-media/media/userimages/userthumbs/", default = 'site-media/user_default_big.jpeg')
    thumbnail2 = models.ImageField(upload_to="site-media/media/userimages/userthumbs2/", default = 'site-media/user_default_small.jpeg')
    pub_date = models.DateTimeField(auto_now_add=True)
    bio = models.CharField(max_length=330, blank = True)
    education = models.CharField(max_length=50, blank = True)
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()

    def __unicode__(self):
        return self.image.name


class StreamItem(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    pub_date = models.DateTimeField()
    user = models.ForeignKey(User)
    
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def get_rendered_html(self):
        template_name = 'streams/stream_item_%s.html' % (self.content_type.name)
        return render_to_string(template_name, { 'object': self.content_object })


def create_stream_item(sender, instance, signal, *args, **kwargs):
    # Check to see if the object was just created for the first time
    if 'created' in kwargs:
        if kwargs['created']:
            create = True
            
            # Get the instance's content type
            ctype = ContentType.objects.get_for_model(instance)

            pub_date = instance.pub_date

            if ctype.name == "photo":
                user = instance.album.user
            else:
                user = instance.user
            
            if create:
                StreamItem.objects.get_or_create(content_type=ctype, object_id=instance.id, pub_date=pub_date, user = user)
  
def delete_stream_item(sender, instance, signal, *args, **kwargs):
    ctype = ContentType.objects.get_for_model(instance)
    StreamItem.objects.filter(content_type=ctype, object_id=instance.id).delete()

# Send a signal on post_save for each of these models
for model in [Fest, Upvote, FestFollowing, CollegeFollowing ]:
    post_save.connect(create_stream_item, sender=model)
    post_delete.connect(delete_stream_item, sender = model)

class FestStreamItem(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    pub_date = models.DateTimeField()
    fest = models.ForeignKey(Fest)
    
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def get_rendered_html(self):
        template_name = 'streams/fest_stream_item_%s.html' % (self.content_type.name)
        return render_to_string(template_name, { 'object': self.content_object })

    def fest_(self):
        return self.fest.name

def create_fest_stream_item(sender, instance, signal, *args, **kwargs):
    if 'created' in kwargs:
        if kwargs['created']:
            create = True
            
            ctype = ContentType.objects.get_for_model(instance)

            pub_date = instance.pub_date
            if ctype.name == "photo":
                fest = instance.album.fest
            elif ctype.name == "fest":
                fest = instance
            else:
                fest = instance.fest
            
            if create:
                FestStreamItem.objects.get_or_create(content_type=ctype, object_id=instance.id, pub_date=pub_date, fest = fest)

def delete_fest_stream_item(sender, instance, signal, *args, **kwargs):
    ctype = ContentType.objects.get_for_model(instance)
    FestStreamItem.objects.filter(content_type=ctype, object_id=instance.id).delete()

# Send a signal on post_save for each of these models
for model in [Event, Fest]:
    post_save.connect(create_fest_stream_item, sender=model)
    post_delete.connect(delete_fest_stream_item, sender = model)






