from __future__ import absolute_import

from celery import shared_task
from fest.models import Event, Fest
from django.core.mail import send_mail

from celery.decorators import periodic_task
from django.utils.timezone import timedelta


@periodic_task(run_every=timedelta(seconds=10))
def event_rank():
    events = Event.objects.all()
    for event in events:
    	event.rank = event.post_rank(event.pub_date, event.upvote_set.count(), event.downvote_set.count())
    	event.save()

@periodic_task(run_every=timedelta(seconds=10))
def fest_rank():
	fests = Fest.objects.all()
  	for fest in fests:
  		events = fest.event_set.all()
  		for event in events:
  			fest.rank = fest.fest_rank(fest.festfollowing_set.count(), fest.pub_date, event.rank)
  			fest.save()

@shared_task
def send_async_mail(w, x, y, z):
  send_mail(w, x, y, z)


