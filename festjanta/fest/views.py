from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth import logout
from django.template import RequestContext
from forms import *
from models import *
from django.contrib.auth.decorators import login_required
import json
from django.core.exceptions import ObjectDoesNotExist
#from itertools import chain
from django.db.models import Q
from django.views.decorators.cache import cache_page
from django.contrib.auth.views import password_reset, password_reset_confirm
from endless_pagination.decorators import page_template
#import dateutil.parser
from django.utils import timezone
import datetime

import random
from hashlib import sha1
from django.core.mail import send_mail
from .tasks import send_async_mail

from django.core.files.uploadedfile import InMemoryUploadedFile
import StringIO
from PIL import ImageOps
from django.utils.html import escape
from django.views.decorators.cache import never_cache
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.sites.models import get_current_site
from django.contrib.auth import login as auth_login

# Return user friendly date and time
def get_date_string(time=False):
    now = timezone.now()
    if type(time) is int:
        diff = now - datetime.datetime.fromtimestamp(time)
    elif isinstance(time,datetime.datetime):
        diff = now - time 
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "Just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return  "a minute ago"
        if second_diff < 3600:
            return str( second_diff / 60 ) + " minutes ago"
        if second_diff < 7200:
            return "about an hour ago"
        if second_diff < 86400:
            return str( second_diff / 3600 ) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff/7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff/30) + " months ago"
    return str(day_diff/365) + " years ago"

# Home page
@page_template('main_page_part.html')
@login_required(login_url='/login/')
def main_page(request,
              template='main_page.html',
              extra_context=None):
  # list of people followed by user
  followings = request.user.profile.follows.all()
  # list of colleges followed by user
  college_followings = CollegeFollowing.objects.filter(user = request.user)
  # list of fests followed by user
  fest_followings = FestFollowing.objects.filter(user = request.user)
  # feed generated for user's following
  userfeed = StreamItem.objects.select_related().filter(user__in = followings)
  # feed generated for user's college and fest following
  festcollegefeed = FestStreamItem.objects.select_related().filter(Q(fest__festfollowing__in = fest_followings) 
                  | Q(fest__college__collegefollowing__in = college_followings)).distinct()
  feeds = list(userfeed) + list(festcollegefeed)
  feeds = sorted(feeds , key=lambda x: x.pub_date, reverse=True)
  # list of trending fests
  fests = Fest.objects.order_by("-rank")[:5]
  context = RequestContext(request, {
    'user' : request.user,
    'feeds' : feeds,
    'page_template': page_template,
    'fests': fests
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))

#-------------------------------------------------------------------------------------------
# User, College, Fest Settings
#-------------------------------------------------------------------------------------------
#check
# Get user profile settings
@login_required(login_url='/login/')
def profile_settings(request):
  colleges = College.objects.filter(user = request.user)
  fests = Fest.objects.filter(user = request.user)
  user_profile = get_object_or_404(UserProfile, user = request.user)
  context = RequestContext(request, {
    'user' : request.user,
    'colleges' : colleges,
    'fests' : fests,
    'user_profile' : user_profile
  })
  return render_to_response('settings/profile_settings.html', context)

#check
# Get user password settings
@login_required(login_url='/login/')
def password_settings(request):
  colleges = College.objects.filter(user = request.user)
  fests = Fest.objects.filter(user = request.user)
  context = RequestContext(request, {
    'user' : request.user,
    'colleges' : colleges,
    'fests' : fests,
  })
  return render_to_response('settings/password_settings.html', context)

# Get college settings
@login_required(login_url='/login/')
def college_settings(request, college_slug):
  colleges = College.objects.filter(user = request.user)
  college = get_object_or_404(College, slug = college_slug)
  fests = Fest.objects.filter(user = request.user)
  if college.user == request.user:
    context = RequestContext(request, {
      'user' : request.user,
      'colleges' : colleges,
      'fests' : fests,
      'college' : college,
    })
  else:
    context = RequestContext(request, {
      'user' : request.user,
      'colleges' : colleges,
      'fests' : fests,
    })
  return render_to_response('settings/college_settings.html', context)

# Get fest settings
@login_required(login_url='/login/')
def fest_settings(request, fest_slug):
  colleges = College.objects.filter(user = request.user)
  fest = get_object_or_404(Fest, slug = fest_slug)
  fests = Fest.objects.filter(user = request.user)
  if fest.user == request.user:
    context = RequestContext(request, {
      'user' : request.user,
      'colleges' : colleges,
      'fests' : fests,
      'fest' : fest,
    })
  else:
    context = RequestContext(request, {
      'user' : request.user,
      'colleges' : colleges,
      'fests' : fests,
    })
  return render_to_response('settings/fest_settings.html', context)

# Get user account settings
@login_required(login_url='/login/')
def user_settings(request):
  colleges = College.objects.filter(user = request.user)
  fests = Fest.objects.filter(user = request.user)
  context = RequestContext(request, {
    'user' : request.user,
    'colleges' : colleges,
    'fests' : fests
  })
  return render_to_response('settings/settings.html', context)

# Edit user, college, fest Settings  
@login_required(login_url='/login/')
def edit_description(request, value, vid):
  if request.method == 'POST':
    if int(value)==1:
      if request.user.check_password(request.POST.get('password')):
        college = get_object_or_404(College, id= vid)
        if college and college.user == request.user:
          college.description = request.POST.get('description')
          college.website = request.POST.get('website')
          college.location = request.POST.get('location')
          college.save()
          return HttpResponse()
      else:
        return HttpResponse("error")
    if int(value)==2:
      if request.user.check_password(request.POST.get('password')):
        fest = get_object_or_404(Fest, id= vid)
        if fest and fest.user == request.user:
          fest.description = request.POST.get('description')
          fest.website = request.POST.get('website')
          fest.save()
          return HttpResponse()
      else:
        return HttpResponse("error")
    if int(value)==3:
      user = get_object_or_404(User, id= vid)
      if user.check_password(request.POST.get('password')):
        if user and user == request.user:
          user.first_name = request.POST.get('first_name')
          user.last_name = request.POST.get('last_name')
          user.save()
          return HttpResponse()
      else:
        return HttpResponse("error")
    if int(value)==4:
      user = get_object_or_404(User, id= vid)
      if user and user == request.user and request.POST.get('newpassword')==request.POST.get('again') and user.check_password(request.POST.get('current')):
        user.set_password(request.POST.get('newpassword'))
        user.save()
        return HttpResponse()
      else:
        return HttpResponse("error")
    if int(value)==5:
      user = get_object_or_404(User, id= vid)
      if user.check_password(request.POST.get('password')):
        if user and user == request.user:
          user_profile = get_object_or_404(UserProfile, id= vid)
          if user_profile and user_profile.user == request.user:
            user_profile.bio = request.POST.get('bio')
            user_profile.education = request.POST.get('education')
            user_profile.save()
            return HttpResponse()
      else:
        return HttpResponse("error")
  if request.method == 'GET':
    return HttpResponseRedirect('/')

#-------------------------------------------------------------------------------------------
# Account Registration(Register/Forgot password) and Session Management(Login/logout)
#-------------------------------------------------------------------------------------------

#login view
@never_cache
def remember_me_login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationRememberMeForm):
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    
    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():
            # Light security check -- make sure redirect_to isn't garbage.
            if not redirect_to or ' ' in redirect_to:
                redirect_to = '/'
                
            # Heavier security check -- redirects to http://example.com should
            # not be allowed, but things like /view/?param=http://example.com
            # should be allowed. This regex checks if there is a '//' *before* a
            # question mark.
            elif '//' in redirect_to and re.match(r'[^\?]*//', redirect_to):
                    redirect_to = '/'
                    
            if not form.cleaned_data.get('remember_me'):
                request.session.set_expiry(0)
                
            # Okay, security checks complete. Log the user in.
            auth_login(request, form.get_user())
            
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
                
            return HttpResponseRedirect(redirect_to)
            
    else:
        form = authentication_form(request)
        
    request.session.set_test_cookie()
    
    current_site = get_current_site(request)
    
    return render_to_response(template_name, {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }, context_instance=RequestContext(request))

# Activation link confirmation 
def confirm(request, activation_key):
  if request.user:
    HttpResponseRedirect('/')
  try:
    user_profile = UserProfile.objects.get(activation_key=activation_key)
    if user_profile.key_expires < timezone.now():
      return render_to_response('registration/confirm.html', {'expired': True})
    user_account = user_profile.user
    user_account.is_active = True
    user_account.save()
    return render_to_response('registration/confirm.html', {'success': True})
  except ObjectDoesNotExist:
    return render_to_response('registration/confirm.html', {'error': True})

# Forgot password reset link
def reset_confirm(request, uidb64=None, token=None):
  return password_reset_confirm(request, template_name='registration/password_reset_confirm.html',
    uidb64=uidb64, token=token, post_reset_redirect='/login/')

# Forgot password get the account email
def reset(request):
  return password_reset(request, template_name='registration/password_reset_form.html',
    email_template_name='registration/password_reset_email.html',
    subject_template_name='registration/password_reset_subject.txt',
    post_reset_redirect='/resetdone/')

# Logout from the account
def logout_page(request):
  logout(request)
  return HttpResponseRedirect('/login/')

# Register for an account and send account activation email
def register_page(request):
  if request.method == 'POST':
    form = RegistrationForm(request.POST, auto_id = False)
    if form.is_valid():
      user = User.objects.create_user(
        username=form.cleaned_data['username'],
        first_name = form.cleaned_data['first_name'].capitalize(),
        last_name = form.cleaned_data['last_name'].capitalize(),
        email=form.cleaned_data['email'],
        password=form.cleaned_data['password1']  
      )
      user.is_active = False
      user.save()
      # Build the activation key for their account                                                                                                                    
      salt = sha1(str(random.random())).hexdigest()[:5]
      activation_key = sha1(salt+user.username).hexdigest()
      key_expires = datetime.datetime.today() + datetime.timedelta(2)

      userprofile = UserProfile(user=user, 
                                activation_key = activation_key,
                                key_expires = key_expires) 
      userprofile.save()

      # Send an email with the confirmation link                                                                                                                      
      email_subject = 'FestJanta account created'
      email_body = """Hello, %s, and thanks for signing up for an                                                                                                    
festjanta.com account!\n\nTo activate your account, click this link within 48                                                                                               
hours:\n\nhttp://example.com/accounts/activate/%s""" % (
        user.username,
        userprofile.activation_key)
      send_mail(email_subject, email_body, 'no-reply@festjanta.com',
        [user.email], fail_silently=True)
      """send_async_mail.delay(email_subject,
                email_body,
                'admin@festjanta.com',
                [user.email])
      """
      return HttpResponseRedirect('/register/success/')
  else:
    form = RegistrationForm(auto_id = False)
  variables = RequestContext(request, {
     'form': form,
     'errors': form.errors
  })
  return render_to_response(
    'registration/register.html', 
    variables
  )

#Check if an email is registered already or username already exists
def get_description(request):
  if request.method == "GET":
    if request.GET['type'].strip() == 'email':
      user =get_object_or_404(User, email=request.GET['email'].strip())
      if user and user != request.user:
        return HttpResponse("True")
      else:
        return HttpResponse("already")
    elif request.GET['type'].strip() == 'username':
      user =get_object_or_404(User, username=request.GET['username'].strip())
      if user and user != request.user:
        return HttpResponse("True")
    elif request.GET['type'].strip() == 'fest':
      fest=get_object_or_404(Fest, pk=request.GET['fest_pk'].strip())
      if fest:
        return HttpResponse(fest.description)
#-------------------------------------------------------------------------------------------
# Ajax autocomplete requests
#-------------------------------------------------------------------------------------------

# college suggestions
@login_required(login_url='/login/')
def ajax_college_autocomplete(request):
  if request.GET.has_key('term'):
    colleges = College.objects.filter(name__istartswith=request.GET['term'])[:4]
    return HttpResponse(json.dumps([college.name for college in colleges]))
  return HttpResponse()

# fest suggestions
@login_required(login_url='/login/')
def ajax_fest_autocomplete(request):
  if request.GET.has_key('term'):
    fests = Fest.objects.filter(name__istartswith=request.GET['term'])[:4]
    return HttpResponse(json.dumps([fest.name for fest in fests]))
  return HttpResponse()

#search-bar (college, fest, user suggestions)
@login_required(login_url='/login/')
def query_suggestions(request):
  if request.GET.has_key('term'):
    colleges = College.objects.filter(name__istartswith = request.GET['term'])[:3]
    fests = Fest.objects.filter(name__istartswith=request.GET['term'])[:3]
    users = User.objects.filter(first_name__istartswith=request.GET['term'])[:3]
    data = []
    json_str = ""
    if users:
      data.extend([{'label': user.first_name, 'value': '/'+ user.username + '/'} for user in users])
    if colleges:
      data.extend([{'label': college.name, 'value': college.get_absolute_url()} for college in colleges])
    if fests:
      data.extend([{'label': fest.name, 'value': fest.get_absolute_url()} for fest in fests])
    json_str = json.dumps(data)
    return HttpResponse(json_str)
  return HttpResponse()

#-------------------------------------------------------------------------------------------
# Fest and college management
#-------------------------------------------------------------------------------------------

#checks for adding a fest
def _fest_save(request, form):
  # Create or get college name.
  try:
    college = College.objects.get(name=form.cleaned_data['college_name'])
  except College.DoesNotExist:
    college = College.objects.create(name=form.cleaned_data['college_name'].capitalize(),
                          user = request.user
                          )
  
  # Create or get fest name.
  try:
    fest = Fest.objects.get(name=form.cleaned_data['name'].capitalize(), 
                            college=college
                           )
  except Fest.DoesNotExist:
    fest = Fest.objects.create(name=form.cleaned_data['name'].capitalize(),
                               user = request.user,
                               college=college
                              )
  return fest

#add a fest and college
@login_required(login_url='/login/')
def fest_save_page(request):
  if request.method == 'POST':
    form = FestSaveForm(request.POST, auto_id = False)
    if form.is_valid():
      fest = _fest_save(request, form)
      return HttpResponseRedirect(
        fest.get_absolute_url()
      )
  else:
    form = FestSaveForm(auto_id = False)
  variables = RequestContext(request, {
    'form': form
  })
  return render_to_response('fest_save.html', variables)


#-------------------------------------------------------------------------------------------
# Ajax updating of pages
#-------------------------------------------------------------------------------------------

#Post a comment and put it in latest comment
@login_required(login_url='/login/')
def post_comments(request, event_id):
  if request.method=="POST":
    if request.POST.get('content'):
      event=get_object_or_404(Event, pk=event_id)
      if event is not None:
        comment = Comment.objects.create(
          content = request.POST.get('content'),
          user = request.user,
          event_id = event_id,
        )
        comment.save()
        date = get_date_string(comment.pub_date)
        text = """ 
          <div class="separate-comment"><img class="fest-image" src="/site-media/%(thumb)s" />
          <a href="/%(username)s/" class="first_name">%(first_name)s</a><br />
          <div class="u-timestamp" title="%(pdate)s" >%(date)s</div>
          <div class="fest-content-event-content-comment"> 
          %(comment)s</div></div>
          """ % {'thumb' :comment.user.person.thumbnail2,
                 'username' :comment.user.username,
                 'first_name' :comment.user.first_name,
                 'pdate' : comment.pub_date,
                 'date' : date,
                 'comment' : escape(comment.content)
          }  
        return HttpResponse(json.dumps([{'html': text}]))
  return HttpResponse()

#Follow a fest
@login_required(login_url='/login/')
def user_follows(request, fest_id):
  if request.method=="POST":
    fest = get_object_or_404(Fest, id= fest_id)
    following, created = FestFollowing.objects.get_or_create(
      user=request.user,
      follows = fest,
    ) 
    return HttpResponse()
  else:
    raise Http404

#Unfollow a fest
@login_required(login_url='/login/')
def user_unfollows(request, fest_id):
  if request.method=="POST":
    fest = get_object_or_404(Fest, id= fest_id)
    FestFollowing.objects.filter(user=request.user,
                                follows = fest,).delete()  
    return HttpResponse()
  else:
    raise Http404

#Follow a college
@login_required(login_url='/login/')
def user_follows_college(request, college_id):
  if request.method=="POST":
    college = get_object_or_404(College, id= college_id)
    following, created = CollegeFollowing.objects.get_or_create(
      user=request.user,
      follows = college,
    ) 
    return HttpResponse()
  else:
    raise Http404

#Unfollow a college
@login_required(login_url='/login/')
def user_unfollows_college(request, college_id):
  if request.method=="POST":
    college = get_object_or_404(College, id= college_id)
    CollegeFollowing.objects.filter(user=request.user,
                                follows = college,).delete()  
    return HttpResponse()
  else:
    raise Http404

#Follow a user
@login_required(login_url='/login/')
def follow(request, user_id):
    if request.method == "POST":
      try:
        user = User.objects.get(id=user_id)
        request.user.profile.follows.add(user.profile)
        return HttpResponse("reached here")
      except ObjectDoesNotExist:
        return HttpResponse("goog")
    return HttpResponse("bad")

#Unfollow a user
@login_required(login_url='/login/')
def unfollow(request, user_id):
    if request.method == "POST":
      try:
        user = User.objects.get(id=user_id)
        request.user.profile.follows.remove(user.profile)
      except ObjectDoesNotExist:
        return HttpResponse()
    return HttpResponse()

#Like an event
@login_required(login_url='/login/')
def like_event(request, vid, event_id):
  if request.method=="POST":
    if int(vid) == 1:
      event = get_object_or_404(Event, id = event_id)
      vote, created = Upvote.objects.get_or_create(user = request.user, 
                                           event = event)
      if created:
        downvote = Downvote.objects.filter(user = request.user, 
                                         event = event)
        if downvote:
          downvote.delete()
      return HttpResponse()
    elif int(vid) == 2:
      event = get_object_or_404(Event, id = event_id)
      vote = get_object_or_404(Upvote, user = request.user, event = event)
      vote.delete()
      return HttpResponse()
  else:
    raise Http404

#Dislike an event
@login_required(login_url='/login/')
def dislike_event(request, vid, event_id):
  if request.method=="POST":
    if int(vid) == 1:
      event = get_object_or_404(Event, id = event_id)
      vote, created = Downvote.objects.get_or_create(user = request.user, 
                                           event = event)
      if created:
        upvote = Upvote.objects.filter(user = request.user, 
                                         event = event)
        if upvote:
          upvote.delete()
      return HttpResponse()
    elif int(vid) == 2:
      event = get_object_or_404(Event, id = event_id)
      vote = get_object_or_404(Downvote, user = request.user, event = event)
      vote.delete()
      return HttpResponse()
  else:
    raise Http404

#Load an event's complete detail
def event_detail(request, event_id):
  comment_no = Comment.objects.filter(event = event_id).count()
  comments = Comment.objects.filter(event = event_id)[:3]
  upvotes = Upvote.objects.filter(event = event_id).count()
  downvotes = Downvote.objects.filter(event = event_id).count()
  try:
    upvoted = Upvote.objects.get(event = event_id, 
                                 user = request.user)
    html = """
        <div class="ld-container2">
        <i class="fi-comments size-18 comment-count"></i><div class="u-comments"> %(comment_no)s </div>
        <a href="/#" eventi="%(event_id)s" class="u-liked"><i class="like fi-like size-18"></i></a>
        <div class="u-upvotes"> %(upvotes)s </div>
        <a href="/#" eventi="%(event_id)s" class="u-dislike"><i class="fi-dislike size-18"></i></a>
        <div class="u-downvotes"> %(downvotes)s </div>
      """ % {'comment_no' :comment_no, 
             'event_id' : event_id, 
             'downvotes' : downvotes , 
             'upvotes' : upvotes}
  except ObjectDoesNotExist:
    try:
      downvoted = Downvote.objects.get(event = event_id, 
                                 user = request.user)
      html = """
        <div class="ld-container2">
        <i class="fi-comments size-18 comment-count"></i><div class="u-comments"> %(comment_no)s </div>
        <a href="/#" eventi="%(event_id)s" class="u-like"><i class="like fi-like size-18"></i></a>
        <div class="u-upvotes"> %(upvotes)s </div>
        <a href="/#" eventi="%(event_id)s" class="u-disliked"><i class="fi-dislike size-18"></i></a>
        <div class="u-downvotes"> %(downvotes)s </div>
      """ % {'comment_no' :comment_no, 
             'event_id' : event_id, 
             'downvotes' : downvotes , 
             'upvotes' : upvotes}
    except ObjectDoesNotExist:
      html = """
        <div class="ld-container2">
        <i class="fi-comments size-18 comment-count"></i><div class="u-comments"> %(comment_no)s </div>
        <a href="/#" eventi="%(event_id)s" class="u-like"><i class="like fi-like size-18"></i></a>
        <div class="u-upvotes"> %(upvotes)s </div>
        <a href="/#" eventi="%(event_id)s" class="u-dislike"><i class="fi-dislike size-18"></i></a>
        <div class="u-downvotes"> %(downvotes)s </div>
      """ % {'comment_no' :comment_no, 
             'event_id' : event_id, 
             'downvotes' : downvotes , 
             'upvotes' : upvotes}

  html +="""
    </div><!-- .comment-bar ends -->
   
    <div class="fest-content-event-comment-post">
      <img class="fest-image" src="/site-media/%(thumbnail)s" />
      <form method="post">
        <textarea class="comment-post-content" name="comment-post-content" cols="55" rows="1" placeholder="Add a comment..."></textarea>
        <input type="hidden" class="event-id" value="%(event_id)s">
        <input type="submit" class="u-post-button" value="Post comment" />
      </form>

    </div><!-- .fest-content-event-comment-post --> 
    <div class="fest-content-event-comment">
    <div class="latest-comment"></div>
  """ % {'thumbnail' :request.user.person.thumbnail2, 
         'event_id' : event_id, 
        }
  
  for comment in comments:
    date = get_date_string(comment.pub_date)
    text = """ 
      <div class="separate-comment"><img class="fest-image" src="/site-media/%(thumb)s" />
      <a href="/%(username)s/" class="first_name">%(first_name)s</a><br />
      <div class="u-timestamp" title="%(date)s" >%(date)s</div>
      <div class="fest-content-event-content-comment"> 
      %(comment)s</div></div>
    """ % {'thumb' : comment.user.person.thumbnail2,
           'username' : comment.user.username,
           'first_name' : comment.user.first_name,
           'date' : date,
           'comment' : escape(comment.content)
          }
    html += text   
  return HttpResponse(json.dumps([{'html': html, 'comment_count' : comment_no}]))

#Make thumbnails for user, college and fest dp
def MakeThumbnail(file, size, size1, size2, username):
    img = Image.open(file)

    imagef = ImageOps.fit(img, size, Image.ANTIALIAS)
    thumbnailString = StringIO.StringIO()
    imagef.save(thumbnailString, 'JPEG')
    image = InMemoryUploadedFile(thumbnailString, None, '%s.%s'%(username,'jpeg'), 'image/jpeg', thumbnailString.len, None)

    imagef = ImageOps.fit(img, size1, Image.ANTIALIAS)
    thumbnailString = StringIO.StringIO()
    imagef.save(thumbnailString, 'JPEG')
    newFile = InMemoryUploadedFile(thumbnailString, None, '%s_big.%s'%(username,'jpeg'), 'image/jpeg', thumbnailString.len, None)

    imagef = ImageOps.fit(img, size2, Image.ANTIALIAS)
    thumbnailString = StringIO.StringIO()
    imagef.save(thumbnailString, 'JPEG')
    newFile2 = InMemoryUploadedFile(thumbnailString, None, '%s_small.%s'%(username,'jpeg'), 'image/jpeg', thumbnailString.len, None)
    return image, newFile, newFile2

# (POST) Upload DP
@login_required(login_url='/login/')
def upload_image(request, vid):
  if request.method=="POST":
    if request.FILES:
      if int(vid) == 1: 
        user=get_object_or_404(UserProfile, user=request.user)
        if user:
          image = request.FILES['file']
          content_type = image.content_type.split('/')[0]
          if content_type == 'image':
            user.image, user.thumbnail, user.thumbnail2 = MakeThumbnail(image, 
            (250, 193), 
            (100, 100), 
            (32, 32), 
            request.user.username)
            user.save()
            return HttpResponse()
          else:
            return HttpResponse('file-type-error') 
      elif int(vid) == 2:
        fest=get_object_or_404(Fest, pk=request.POST.get('fest_pk'))
        if fest:
          image = request.FILES['file']
          content_type = image.content_type.split('/')[0]
          if content_type == 'image':
            fest.image, fest.thumbnail, fest.thumbnail2 = MakeThumbnail(image, 
              (250, 193), 
              (100, 100), 
              (32, 32), 
              fest.name)
            fest.save()
            return HttpResponse()
          else:
            return HttpResponse('file-type-error')  
      elif int(vid) == 3:
        college=get_object_or_404(College, pk=request.POST.get('college_pk'))
        if college:
          image = request.FILES['file']
          content_type = image.content_type.split('/')[0]
          if content_type == 'image':
            college.image, college.thumbnail, college.thumbnail2 = MakeThumbnail(image, 
              (250, 193), 
              (100, 100), 
              (32, 32), 
              college.name)
            college.save()
            return HttpResponse()
          else:
            return HttpResponse('file-type-error')     
  return HttpResponse()

# (Get) Get uploaded user, college or fest dp
@login_required(login_url='/login/')
def get_uploaded_image(request):
  if request.method == "GET":
    if request.GET['type'].strip() == 'user':
      user=get_object_or_404(UserProfile, user=request.user)
      if user:
        return HttpResponse(user)
    elif request.GET['type'].strip() == 'college':
      college=get_object_or_404(College, pk=request.GET['college_pk'].strip())
      if college:
        return HttpResponse(college)
    elif request.GET['type'].strip() == 'fest':
      fest=get_object_or_404(Fest, pk=request.GET['fest_pk'].strip())
      if fest:
        return HttpResponse(fest)


# (Post) Upload an album image 
@login_required(login_url='/login/')
def upload_event_album(request):
  if request.method=="POST":
    if request.FILES:
      content = request.FILES['file']
      content_type = content.content_type.split('/')[0]
      if content_type == "image":
        if content._size > 3145728:
          return HttpResponse('size-error')
        else:
          if request.POST.get('new')=="true":
            fest=get_object_or_404(Fest, pk=request.POST.get('fest_pk'))
            event = Event.objects.create(
              user = request.user,
              fest = fest,
              description = request.POST.get('album_description')
            )
            if event:
              photo = Photo.objects.create(
                image = request.FILES['file'],
                album = event,
              )
              global counter
              counter = 1
              return HttpResponse(json.dumps([event.pk, photo.pk]))
          else:
            if counter < 11:
              photo = Photo.objects.create(
                image = request.FILES['file'],
                album_id = request.POST.get('event_id'),
              )
              counter += 1
              return HttpResponse(json.dumps([request.POST.get('event_id'), photo.pk])) 
            else:
              counter = 0
              return HttpResponse("max-limit-reached") 
      else:
        return HttpResponse('file-type-error')
  return HttpResponse()

# (GET) Get thumbnail of an image uploaded in an album
@login_required(login_url='/login/')
def uploaded_event_image(request):
  if request.method == "GET":
    photo_pk = request.GET['photo_pk'].strip()
    photo=get_object_or_404(Photo, pk=photo_pk)
    return HttpResponse(photo.thumbnail.url[21:])

# (GET) Get an album and put it in latest event
@login_required(login_url='/login/')
def uploaded_event_album(request, event_id):
  if request.method == "POST":
    try:
      event = Event.objects.get(pk = event_id)
      event.description = request.POST['content']
      event.save()
      photos = event.photo_set.all()
      count = event.photo_set.all().count()
      date = get_date_string(event.pub_date)
      text = """ 
          <div class="fest-content">
          <div class="fest-content-event">
          <a href="#" class="delete-post" eid="%(event_id)s"><i class="fi-x size-10"></i></a>
          <img class="fest-image" src="/site-media/%(fest_thumb)s" />
          <a href="%(url)s" class="event-username">%(fest_name)s</a>
          
          <div class="u-timestamp" title="%(pub_date)s">
          <a href="fest/%(fest_slug)s/post/%(event_id)s/">
          <i class="fi-clock size-14">
          </i> %(date)s</a>
          </div><!-- .u-timestamp --> 
          
          <div class="fest-content-event-content">
          <div class="fest-content-event-content-title">
          <div class="photo-count"> %(count)s <i class="fi-photo size-10"></i> </div>
          </div><!-- .fest-content-event-content-title ends -->
          %(description)s<br>
          
      """ % { 'fest_thumb' : event.fest.thumbnail2,
            'url' : event.fest.get_absolute_url,
            'fest_name' : event.fest.name,
            'fest_slug' : event.fest.slug,
            'event_id' : event.id,
            'pub_date' : event.pub_date,
            'date' : date,
            'count' : count,
            'description' : escape(event.description),
          } 
      for photo in photos[:2]:
        html = """<a href="/site-media/%(image)s" class="event-album album-group-%(event_id)s">
          <img class="fest-content-event-content-photothumb" src="/site-media/%(thumb)s" /></a>
      """%{'image' : photo.image,
           'event_id' : event.id,
           'thumb' : photo.thumbnail,
          }
        text += html

      for photo in photos[2:]:
        html = """<a href="/site-media/%(image)s" class="event-album album-group-%(event_id)s" style="display: none;"></a>
      """%{'image' : photo.image,
           'event_id' : event.id,
          }
        text += html

      text += """</div><!-- .fest-content-event-content ends --> 
          <a class="expand" href="/#" eid="%(event_id)s">Expand</a>
          <div class="comment-expand">
          </div>  
          </div>
          </div>
      """ %{'event_id' : event.id}

      return HttpResponse(json.dumps([{'html': text}]))
    except ObjectDoesNotExist:
      fest=get_object_or_404(Fest, pk=request.POST['fest_id'])
      if fest is not None:
        event = Event.objects.create(
          description = request.POST.get('content'),
          user = request.user,
          fest = fest,
        )
        date = get_date_string(event.pub_date)
        
        text = """ 
          <div class="fest-content">
          <div class="fest-content-event">
          <a href="#" class="delete-post" eid="%(eid)s"><i class="fi-x size-10"></i></a>
          <img class="fest-image" src="/site-media/%(thumb)s" />
          <a href="%(url)s" class="event-username">%(name)s</a>
          
          <div class="u-timestamp">
            <a href="fest/%(event_url)s/post/%(eid)s/"><i class="fi-clock size-14"></i>
              %(date)s
            </a>
          </div><!-- .u-timestamp --> 
          <div class="fest-content-event-content">
          %(description)s
          
          </div><!-- .fest-content-event-content ends --> 

          <a class="expand" href="/#" eid="%(eid)s">Expand</a>
     
          <div class="comment-expand">
          </div>  
          </div>
          </div>
        """ % {'thumb' : event.fest.thumbnail2,
               'url' : event.fest.get_absolute_url,
               'name' : event.fest.name,
               'event_url' :  event.fest.slug,
               'eid' : event.id,
               'date' :  date,
               'description' : escape(event.description),
          } 
        return HttpResponse(json.dumps([{'html': text}]))
      

@login_required(login_url='/login/')
def delete_event(request, event_id):
  if request.method == "POST":
    try:
      event = Event.objects.get(pk = event_id)
      if event.user == request.user:
        event.delete()
      return HttpResponse("success")
    except ObjectDoesNotExist:
      return HttpResponse("Failure")

#-------------------------------------------------------------------------------------------
# College/fest specific pages
#-------------------------------------------------------------------------------------------
#***************************
# Fest Pages
#***************************
# List of followers of a fest
@page_template('fest_following_part.html')
def fest_followers(request, fest_slug,
                   template='fest_following.html',
                   extra_context=None):
  fest = get_object_or_404(Fest, slug= fest_slug)
  if fest:
    editable = False
    fest_followers = FestFollowing.objects.filter(follows = fest)
    try:
      FestFollowing.objects.get(follows = fest, user = request.user)
      follows = True
    except ObjectDoesNotExist:
      follows = False
    if request.user == fest.user:
      editable = True
    context = RequestContext(request, {
      'fest' : fest,
      'follows' : follows,
      'editable' : editable,
      'fest_followers' : fest_followers,
    })
    if extra_context is not None:
      context.update(extra_context)
    return render_to_response(
      template, context, context_instance=RequestContext(request))

# Shows a single event a fest
@page_template('event_page_part.html')
def event_page(request, fest_slug, event_id,
               template='event_page.html',
               extra_context=None):
  try:
    event = Event.objects.select_related('title', 'fest').get(id=event_id)
  except ObjectDoesNotExist:
    raise Http404
  comments = event.comment_set.only('id', 'content', 'user', 'pub_date', 'user')
  upvoted = ""
  downvoted = ""
  if request.user.is_authenticated():
    upvoted = Upvote.objects.filter(user=request.user, event = event)
    downvoted = Downvote.objects.filter(user=request.user, event = event)
  context = RequestContext(request, {
    'event' : event,
    'comments' : comments,
    'upvoted' : upvoted,
    'downvoted' : downvoted,
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))
  

# Fest page
@login_required(login_url='/login/')
@page_template('fest_page_part.html')
def fest_page(request, fest_slug,
              template='fest_page.html',
              extra_context=None):
  fest = get_object_or_404(Fest, slug= fest_slug)
  upvotes = ""
  downvotes = ""
  if fest:
    follows = False
    editable = False
    events = fest.event_set.order_by('-pub_date')
    trends = Fest.objects.order_by("-rank")[:5]
    if request.user.is_authenticated():
      following = FestFollowing.objects.filter(user = request.user,
                                             follows = fest)
      upvotes = Upvote.objects.filter(user=request.user)
      upvotes = upvotes.filter(event__in=events)
      upvotes = upvotes.values_list('event', flat=True)

      downvotes = Downvote.objects.filter(user=request.user)
      downvotes = downvotes.filter(event__in=events)
      downvotes = downvotes.values_list('event', flat=True)
      if following:
        follows = True
      if request.user == fest.user:
        editable = True
      if request.GET.has_key('gis'):
        if request.GET['gis'] == 'hnt':
          events = fest.event_set.order_by('-rank')
        elif request.GET['gis'] == 'lef':
          events = fest.event_set.order_by('-pub_date')
    context = RequestContext(request, {
      'fest' : fest,
      'events' : events,
      'follows' : follows,
      'editable' : editable,
      'upvotes' : upvotes,
      'downvotes' : downvotes,
      'trends' : trends,
    })
    if extra_context is not None:
      context.update(extra_context)
    return render_to_response(
      template, context, context_instance=RequestContext(request))

#***************************
# College Pages
#***************************
# List of followers of a college
@page_template('college_following_part.html')
def college_followers(request, college_slug,
                      template='college_following.html',
                      extra_context=None):
  college = get_object_or_404(College, slug= college_slug)
  if college:
    fests = college.fest_set.all()
    follows = False
    editable = False
    following = ""
    fest_followings = ""
    college_followers = CollegeFollowing.objects.filter(follows = college)
    trends = Fest.objects.order_by("-rank")[:5]
    if request.user.is_authenticated():
      fest_followings = FestFollowing.objects.filter(user = request.user)
      fest_followings = fest_followings.filter(follows__in = fests)
      fest_followings = fest_followings.values_list('follows', flat = True)
      following = CollegeFollowing.objects.filter(user = request.user,
                                             follows = college)
    if following:
      follows = True
    if request.user == college.user:
      editable = True
    context = RequestContext(request, {
      'fests' : fests,
      'college' : college,
      'follows' : follows,
      'editable' : editable,
      'college_followers' : college_followers,
      'fest_followings' : fest_followings,
      'trends' : trends,
    })
    if extra_context is not None:
      context.update(extra_context)
    return render_to_response(
      template, context, context_instance=RequestContext(request))

# College page
def college_page(request, slug):
  college = get_object_or_404(College, slug = slug)
  if college:
    follows = False
    college_followings = CollegeFollowing.objects.filter(follows = college).count()
    fests = college.fest_set.order_by('-id')
    fest_followings = ""
    following = ""
    trends = Fest.objects.order_by("-rank")[:5]
    if request.user.is_authenticated(): 
      following = CollegeFollowing.objects.filter(user = request.user,
                                             follows = college)
      fest_followings = FestFollowing.objects.filter(user = request.user)
      fest_followings = fest_followings.filter(follows__in = fests)
      fest_followings = fest_followings.values_list('follows', flat = True)
    if request.user == college.user:
      editable = True
    else:
      editable = False
    if following:
      follows = True
    variables = RequestContext(request, {
    'college': college,
    'fests' : fests,
    'college_followings' : college_followings,
    'follows' : follows,
    'editable' : editable,
    'fest_followings' : fest_followings,
    'trends': trends,
    })
  return render_to_response('college_page.html', variables)

#-------------------------------------------------------------------------------------------
# Info pages
#-------------------------------------------------------------------------------------------
# About page
def about(request):
  variables = RequestContext(request, {
  })
  return render_to_response('about.html', variables)

# (POST)Contact/FeedBack page
#send email
def feedback(request):
  if request.method == 'POST':
    name = request.POST['name']
    phone = request.POST['phone']
    email = request.POST['email']
    message = request.POST['message']

    # Send an email with feedback                                                                                                                      
    email_subject = 'Feedback/Contact'
    email_body = """ 
      Name: %s, /nPhone: %s, /nEmail: %s, /nMessage: %s,/n
    """ %(name, phone, email, message)
    send_mail(email_subject, email_body, 'no-reply@festjanta.com',
        ['festjantatestserver@gmail.com'], fail_silently=True)
    """send_async_mail.delay(email_subject,
              email_body,
              'admin@festjanta.com',
              ['monicaphalswal@gmail.com'])
    """
    return HttpResponse('success')
  else:
    return HttpResponse('error')

#-------------------------------------------------------------------------------------------
# User specific pages
#-------------------------------------------------------------------------------------------
# Main User Page
@page_template('user_page_part.html')
def user_page(request, username,
              template='user_page.html',
              extra_context=None):
  follows = False
  user = get_object_or_404(User, username=username)
  college_followings = CollegeFollowing.objects.filter(user = user)
  fest_followings = FestFollowing.objects.filter(user = user)
  user_followings = user.profile.followed_by.all()
  following_users = user.profile.follows.all()
  trends = Fest.objects.order_by("-rank")[:5]
  if request.user.is_authenticated(): 
    if request.user.profile.follows.filter(user__username = user):
      follows = True
  feeds = StreamItem.objects.filter(user = user).order_by("-pub_date")
  if request.user == user:
    editable = True
  else:
    editable = False
  
  context = RequestContext(request, {
    'name': user,
    'college_followings' : college_followings,
    'fest_followings' : fest_followings,
    'editable' : editable,
    'follows' : follows,
    'followers': user_followings,
    'followings' : following_users,
    'feeds' : feeds,
    'trends' : trends,
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))

# User's followers page
@page_template('user_followers_part.html')
def user_followers(request, username,
                   template='user_followers.html',
                   extra_context=None):
  follows = False
  user = get_object_or_404(User, username=username)
  college_followings = CollegeFollowing.objects.filter(user = user)
  fest_followings = FestFollowing.objects.filter(user = user)
  user_followings = UserFollowing.objects.filter(follows = user)
  following_users = UserFollowing.objects.filter(followed_by = user)

  trends = Fest.objects.order_by("-rank")[:5]
  if request.user.is_authenticated(): 
    following = UserFollowing.objects.filter(user = request.user,
                                           follows = user)
    if following:
      follows = True
  if request.user == user:
    editable = True
  else:
    editable = False
  
  context = RequestContext(request, {
    'name': user,
    'college_followings' : college_followings,
    'fest_followings' : fest_followings,
    'editable' : editable,
    'follows' : follows,
    'followers': user_followings,
    'followings' : following_users,
    'trends' : trends,
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))

# User's Following page
@page_template('user_following_part.html')
def user_following(request, username,
                   template='user_following.html',
                   extra_context=None):
  follows = False
  user = get_object_or_404(User, username=username)
  trends = Fest.objects.order_by("-rank")[:5]
  college_followings = CollegeFollowing.objects.filter(user = user)
  fest_followings = FestFollowing.objects.filter(user = user)
  user_followings = UserFollowing.objects.filter(follows = user)
  following_users = UserFollowing.objects.filter(followed_by = user)
  if request.user.is_authenticated(): 
    following = UserFollowing.objects.filter(user = request.user,
                                           follows = user)
    if following:
      follows = True
  if request.user == user:
    editable = True
  else:
    editable = False
  
  context = RequestContext(request, {
    'name': user,
    'college_followings' : college_followings,
    'fest_followings' : fest_followings,
    'editable' : editable,
    'follows' : follows,
    'followers': user_followings,
    'followings' : following_users,
    'trends' : trends,
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))

# User's College Following page
@page_template('user_college_following_part.html')
def user_college_following(request, username,
                           template='user_college_following.html',
                           extra_context=None):
  follows = False
  user = get_object_or_404(User, username=username)
  trends = Fest.objects.order_by("-rank")[:5]
  college_followings = CollegeFollowing.objects.filter(user = user)
  fest_followings = FestFollowing.objects.filter(user = user)
  user_followings = UserFollowing.objects.filter(follows = user)
  following_users = UserFollowing.objects.filter(followed_by = user)
  if request.user.is_authenticated(): 
    following = UserFollowing.objects.filter(user = request.user,
                                           follows = user)
    if following:
      follows = True
  if request.user == user:
    editable = True
  else:
    editable = False
  
  context = RequestContext(request, {
    'name': user,
    'college_followings' : college_followings,
    'fest_followings' : fest_followings,
    'editable' : editable,
    'follows' : follows,
    'followers': user_followings,
    'followings' : following_users,
    'trends' : trends,
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))

# User's Fest Following page
@page_template('user_fest_following_part.html')
def user_fest_following(request, username,
                        template='user_fest_following.html',
                        extra_context=None):
  follows = False
  user = get_object_or_404(User, username=username)
  trends = Fest.objects.order_by("-rank")[:5]
  college_followings = CollegeFollowing.objects.filter(user = user)
  fest_followings = FestFollowing.objects.filter(user = user)
  user_followings = UserFollowing.objects.filter(follows = user)
  following_users = UserFollowing.objects.filter(followed_by = user)

  if request.user.is_authenticated(): 
    following = UserFollowing.objects.filter(user = request.user,
                                           follows = user)
    if following:
      follows = True
  if request.user == user:
    editable = True
  else:
    editable = False
  
  context = RequestContext(request, {
    'name': user,
    'college_followings' : college_followings,
    'fest_followings' : fest_followings,
    'editable' : editable,
    'follows' : follows,
    'followers': user_followings,
    'followings' : following_users,
    'trends' : trends,
  })
  if extra_context is not None:
    context.update(extra_context)
  return render_to_response(
    template, context, context_instance=RequestContext(request))


